import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class ChooseDomisili {
	
	public static void main(String[] args) throws MalformedURLException, InterruptedException {
		// TODO Auto-generated method stub
		
		//define desired capabilities for send key and value to server appium
		DesiredCapabilities dc = new DesiredCapabilities();
		
		dc.setCapability(MobileCapabilityType.DEVICE_NAME, "Galaxy A6");
		dc.setCapability("platformName", "Android");
		//select your apk file location
		dc.setCapability("app", "C:\\ApkFile\\181 sampingan.apk");
		dc.setCapability("appPackage", "com.sampingan.agentapp");
		dc.setCapability("appActivity", "com.sampingan.agentapp.activities.SplashActivity");
		
		//creates a new instance based on appium local driver
		AndroidDriver<AndroidElement> ad = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"),dc);
		
		//after splash screen wait until 5 second 
		Thread.sleep(5000);
		
		//action
		MobileElement el1 = (MobileElement) ad.findElementById("com.sampingan.agentapp.auth:id/laySelectDomicile");
		el1.click();
		MobileElement el2 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[5]/android.widget.LinearLayout");
		el2.click();
		MobileElement el3 = (MobileElement) ad.findElementById("com.sampingan.agentapp:id/buttonApply");
		el3.click();
		MobileElement el4 = (MobileElement) ad.findElementById("com.sampingan.agentapp.auth:id/btnContinue");
		el4.click();
		Thread.sleep(2000);
		MobileElement el5 = (MobileElement) ad.findElementById("com.android.permissioncontroller:id/permission_allow_button");
		el5.click();
		Thread.sleep(2000);
		MobileElement el6 = (MobileElement) ad.findElementById("com.sampingan.agentapp.auth:id/buttonSkip");
		el6.click();

	}
	


}
