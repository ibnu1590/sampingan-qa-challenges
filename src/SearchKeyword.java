import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class SearchKeyword {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "D:\\Automation\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();	
		
		//declaration of string array without size
		String[] arrKeyWord = {};	
		//declaration of integer without value
		int totalKeyWord = 0;
		
		driver.get("https://sampingan.co.id/learn/");		
		Thread.sleep(2000);
		
		//convert element to list element
		List<WebElement> key = driver.findElements(By.xpath("(//*[contains(translate(text(), 'Kerja', 'kerja'), 'kerja')])"));
		
		//adding all the elements to Array
		for (int i=0; i < key.size(); i++) {
			//add data keyword to String
			String isiKey = key.get(i).getText();
			//convert array to array list
			List<String> arrList = new ArrayList<String>(Arrays.asList(arrKeyWord)); 
			//add keyword to array list
			arrList.add(isiKey);
			//revert conversion from array list to array 
			arrKeyWord = arrList.toArray(arrKeyWord);
			//add keyword length to integer totalKeyWord
			totalKeyWord = arrKeyWord.length;
		}
		
		//print total length array 
		System.out.println("Total keyword �kerja� > " + totalKeyWord);
	}
}
